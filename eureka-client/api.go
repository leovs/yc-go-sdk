package eureka_client

import (
	"fmt"
	"github.com/xuanbo/requests"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// 与eureka服务端rest交互
// https://github.com/Netflix/eureka/wiki/Eureka-REST-operations

// Register 注册实例
// POST /eureka/v2/apps/appID
func Register(zone, app string, instance *Instance) error {
	// Instance 服务实例
	type InstanceInfo struct {
		Instance *Instance `json:"instance"`
	}
	var info = &InstanceInfo{
		Instance: instance,
	}

	u := zone + "apps/" + app
	result := requests.Post(u).Json(info).Send().Status2xx()
	defer closeRequest(result.Resp)
	if result.Err != nil {
		fmt.Printf("Register application instance failed, error: %s \n", result.Err)
		time.Sleep(10 * time.Second)
		return Register(zone, app, instance)
	}
	return nil
}

// UnRegister 删除实例
// DELETE /eureka/v2/apps/appID/instanceID
func UnRegister(zone, app, instanceID string) error {
	u := zone + "apps/" + app + "/" + instanceID
	result := requests.Delete(u).Send().StatusOk()
	defer closeRequest(result.Resp)
	if result.Err != nil {
		fmt.Printf("UnRegister application instance failed, error: %s \n", result.Err)
		return result.Err
	}
	return nil
}

// Refresh 查询所有服务实例
// GET /eureka/v2/apps
func Refresh(zone string) (*Applications, error) {
	type Result struct {
		Applications *Applications `json:"applications"`
	}
	apps := new(Applications)
	res := &Result{
		Applications: apps,
	}
	u := zone + "apps"
	err := requests.Get(u).Header("Accept", " application/json").Send().StatusOk().Json(res)
	if err != nil {
		return nil, fmt.Errorf("refresh failed, error: %s", err)
	}
	return apps, nil
}

// Heartbeat 发送心跳
// PUT /eureka/v2/apps/appID/instanceID
func Heartbeat(zone, app, instanceID string) error {
	u := zone + "apps/" + app + "/" + instanceID
	params := url.Values{
		"status":             {"UP"},
		"lastDirtyTimestamp": {strconv.FormatInt(time.Now().Unix(), 10)},
	}
	result := requests.Put(u).Params(params).Send().StatusOk()
	defer closeRequest(result.Resp)
	if result.Err != nil {
		return fmt.Errorf("heartbeat failed, error: %s", result.Err)
	}
	return nil
}

// 关闭请求线程
func closeRequest(response *http.Response) {
	if !response.Close && response.Body != nil {
		_ = response.Body.Close()
	}
}
