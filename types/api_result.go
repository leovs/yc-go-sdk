package types

import "github.com/gofiber/fiber/v2"

type IApiResult interface {
	Body() error
	SetContext(ctx *fiber.Ctx)
	SetValue(value any)
}

type result struct {
	Value   any
	Context *fiber.Ctx
}

func (r *result) SetContext(ctx *fiber.Ctx) {
	r.Context = ctx
}

func (r *result) SetValue(value any) {
	r.Value = value
}

type XmlResult struct {
	result
}

func (r *XmlResult) Body() error {
	return r.Context.XML(r.Value)
}

type JsonResult struct {
	result
}

func (r *JsonResult) Body() error {
	return r.Context.JSON(r.Value)
}

type JsonpResult struct {
	result
}

func (r *JsonpResult) Body() error {
	return r.Context.JSONP(r.Value)
}

type StringResult struct {
	result
}

func (r *StringResult) Body() error {
	return r.Context.SendString(r.Value.(string))
}

type NullResult struct {
	result
}

func (r *NullResult) Body() error {
	return nil
}

type HttpCodeResult struct {
	result
}

func (r *HttpCodeResult) Body() error {
	return r.Context.SendStatus(r.Value.(int))
}

type RedirectResult struct {
	result
}

func (r *RedirectResult) Body() error {
	return r.Context.Redirect(r.Value.(string))
}

type ViewResult struct {
	result
	ViewName string
}

func (r *ViewResult) Body() error {
	return r.Context.Render(r.ViewName, r.Value)
}
