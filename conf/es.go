// Copyright 2023 ztlcloud.com
// leovs @2023.12.12

package conf

import (
	"gitee.com/leovs/yc-go-sdk/sdk"
	"github.com/elastic/elastic-transport-go/v8/elastictransport"
	"github.com/elastic/go-elasticsearch/v8"
	"log"
	"os"
)

type EsConfig struct {
	Addresses             []string `yaml:"addresses"`             // 地址
	Username              string   `yaml:"username"`              // 用户名
	Password              string   `yaml:"password"`              // 密码
	EnableRequestBodyLog  bool     `yaml:"enableRequestBodyLog"`  // 是否开启调试日志
	EnableResponseBodyLog bool     `yaml:"enableResponseBodyLog"` // 是否开启调试日志
}

// Init 初始化配置
func (e *EsConfig) Init(_config *Settings) {
	// 检查是否配置了ES
	if e == nil || _config.EsConfig == nil || _config.EsConfig.Addresses == nil || len(_config.EsConfig.Addresses) <= 0 {
		return
	}

	config := elasticsearch.Config{
		Addresses: _config.EsConfig.Addresses,
		Username:  _config.EsConfig.Username,
		Password:  _config.EsConfig.Password,
	}

	if _config.EsConfig.EnableRequestBodyLog || _config.EsConfig.EnableResponseBodyLog {
		config.Logger = &elastictransport.ColorLogger{
			Output:             os.Stdout,
			EnableRequestBody:  _config.EsConfig.EnableRequestBodyLog,
			EnableResponseBody: _config.EsConfig.EnableResponseBodyLog,
		}
	}

	client, err := elasticsearch.NewTypedClient(config)
	if err != nil {
		log.Panicf("ES连接失败 %v\n", err.Error())
	}
	log.Printf("[%v] 连接ES成功\n", _config.AppName)
	sdk.Runtime.SetEs(client)
}
