// Copyright 2023 ztlcloud.com
// leovs @2023.12.12

package log

import (
	"fmt"
	"gitee.com/leovs/yc-go-sdk/errors"
	"time"
)

var DebugMode = false

const (
	infoTag  = "INFO"
	errorTag = "ERROR"
	debugTag = "DEBUG"
)

func timeNow() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func Println(a ...interface{}) {
	fmt.Println(a...)
}

func Printf(format string, a ...interface{}) {
	fmt.Printf(format, a...)
}

func Error(format string, a ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s [%s] %s\n", timeNow(), errorTag, format), a...)
}

func ErrorMessage(err *errors.Message) {
	fmt.Printf(fmt.Sprintf("%s [%s] %s#%d\n", timeNow(), errorTag, err.Msg, err.Code))
}

func Info(format string, a ...interface{}) {
	fmt.Printf(fmt.Sprintf("%s [%s] %s\n", timeNow(), infoTag, format), a...)
}

func Debug(format string, a ...interface{}) {
	if DebugMode {
		fmt.Printf(fmt.Sprintf("%s [%s] %s\n", timeNow(), debugTag, format), a...)
	}
}
func Panic(format string, a ...interface{}) {
	panic(fmt.Sprintf(format, a...))
}
