package runtime

import (
	redisClient "gitee.com/leovs/yc-go-sdk/redis-client"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"sync"
)

const debugFlag = "debug"

type Application struct {
	dbs     map[string]*gorm.DB
	es      *elasticsearch.TypedClient
	redis   *redisClient.RedisClient
	engine  *fiber.App
	mux     sync.RWMutex
	configs map[string]interface{} // 系统参数
	mode    string                 // 运行模式
}

func (e *Application) IsDebug() bool {
	return e.mode == debugFlag
}

func (e *Application) Mode(value ...string) string {
	if len(value) > 0 {
		e.mode = value[0]
	}
	return e.mode
}

// SetDb 设置对应key的db
func (e *Application) SetDb(db *gorm.DB) {
	e.SetDynamicDb("__MAIN", db)
}

// GetDb 获取所有map里的db数据
func (e *Application) GetDb() *gorm.DB {
	return e.GetDynamicDb("__MAIN")
}

// SetDynamicDb 设置对应key的db
func (e *Application) SetDynamicDb(name string, db *gorm.DB) {
	e.mux.Lock()
	defer e.mux.Unlock()
	e.dbs[name] = db
}

// GetDynamicDb 获取所有map里的db数据
func (e *Application) GetDynamicDb(name string) *gorm.DB {
	e.mux.Lock()
	defer e.mux.Unlock()
	return e.dbs[name]
}

// SetEs 设置对应key的es
func (e *Application) SetEs(es *elasticsearch.TypedClient) {
	e.mux.Lock()
	defer e.mux.Unlock()
	e.es = es
}

func (e *Application) GetEs() *elasticsearch.TypedClient {
	e.mux.Lock()
	defer e.mux.Unlock()
	return e.es
}

// SetEngine 设置路由引擎
func (e *Application) SetEngine(engine *fiber.App) {
	e.engine = engine
}

// GetEngine 获取路由引擎
func (e *Application) GetEngine() *fiber.App {
	return e.engine
}

func (e *Application) SetConfig(key string, value interface{}) {
	e.mux.Lock()
	defer e.mux.Unlock()
	e.configs[key] = value
}

func (e *Application) GetConfig(key string) interface{} {
	e.mux.Lock()
	defer e.mux.Unlock()
	return e.configs[key]
}

func (e *Application) SetRedis(redis *redisClient.RedisClient) {
	e.mux.Lock()
	defer e.mux.Unlock()
	e.redis = redis
}
func (e *Application) GetRedis() *redisClient.RedisClient {
	e.mux.Lock()
	defer e.mux.Unlock()
	return e.redis
}

// NewConfig 默认值
func NewConfig() *Application {
	return &Application{
		dbs:     make(map[string]*gorm.DB),
		configs: make(map[string]interface{}),
	}
}
