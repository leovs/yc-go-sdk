package middleware

import (
	"gitee.com/leovs/yc-go-sdk/sdk"
	"github.com/gofiber/fiber/v2"
)

// WithContextDb 数据库链接
func WithContextDb(c *fiber.Ctx) error {
	if db := sdk.Runtime.GetDb(); db != nil {
		c.Locals("db", db.WithContext(c.Context()))
	}
	return c.Next()
}

// InitMiddleware 初始化中间件
func InitMiddleware(r *fiber.App) {
	// 数据库链接
	r.Use(WithContextDb)
}
